# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
import argparse
import glob
import multiprocessing as mp
import os
import time

import cv2
import tqdm
from adet.config import get_cfg
from detectron2.data.detection_utils import read_image
from detectron2.utils.logger import setup_logger
from adet.data import text
from predictor import VisualizationDemo

import numpy as np
from detectron2.utils.visualizer import Visualizer
import sys

# constants
WINDOW_NAME = "COCO detections"


def setup_cfg(args):
    # load config from file and command-line arguments
    cfg = get_cfg()
    cfg.merge_from_file(args.config_file)
    cfg.merge_from_list(args.opts)
    # Set score_threshold for builtin models
    cfg.MODEL.RETINANET.SCORE_THRESH_TEST = args.confidence_threshold
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = args.confidence_threshold
    cfg.MODEL.FCOS.INFERENCE_TH_TEST = args.confidence_threshold
    cfg.MODEL.PANOPTIC_FPN.COMBINE.INSTANCES_CONFIDENCE_THRESH = args.confidence_threshold
    cfg.freeze()
    return cfg

# My FUNCTIONS


def Get1LineResult(image_name, point, text):
    # return str(image_name) + "," + str(point[0][0]) + "," + str(point[0][1]) + "," + str(point[1][0]) + "," + str(point[1][1]) + "," + str(point[2][0]) + "," + str(point[2][1]) + "," + str(point[3][0]) + "," + str(point[3][1]) + "," + text + "\n"
    return str(point[0][0]) + "," + str(point[0][1]) + "," + str(point[1][0]) + "," + str(point[1][1]) + "," + str(point[2][0]) + "," + str(point[2][1]) + "," + str(point[3][0]) + "," + str(point[3][1]) + "," + text + "\n"


def order_points(pts):
    # initialzie a list of coordinates that will be ordered
    # such that the first entry in the list is the top-left,
    # the second entry is the top-right, the third is the
    # bottom-right, and the fourth is the bottom-left
    rect = np.zeros((4, 2), dtype="float32")
    # the top-left point will have the smallest sum, whereas
    # the bottom-right point will have the largest sum
    s = pts.sum(axis=1)
    rect[0] = pts[np.argmin(s)]
    rect[2] = pts[np.argmax(s)]
    # now, compute the difference between the points, the
    # top-right point will have the smallest difference,
    # whereas the bottom-left will have the largest difference
    diff = np.diff(pts, axis=1)
    rect[1] = pts[np.argmin(diff)]
    rect[3] = pts[np.argmax(diff)]
    # return the ordered coordinates
    return np.int32(rect)


def Submit(path, points, texts, out_file):
    image_name = path.split("/")[-1]
    for point, text in zip(points, texts):
        # print(text)
        rect = order_points(point)
        # Convert to uppercase
        upperCaseText = text.upper()
        out_file.write(Get1LineResult(image_name, rect, upperCaseText))


def bezier_to_polygon(bezier):
    u = np.linspace(0, 1, 20)
    bezier = bezier.reshape(2, 4, 2).transpose(0, 2, 1).reshape(4, 4)
    points = (
        np.outer((1 - u) ** 3, bezier[:, 0])
        + np.outer(3 * u * ((1 - u) ** 2), bezier[:, 1])
        + np.outer(3 * (u ** 2) * (1 - u), bezier[:, 2])
        + np.outer(u ** 3, bezier[:, 3])
    )

    # convert points to polygon
    points = np.concatenate((points[:, :2], points[:, 2:]), axis=0)
    return points.tolist()


def _bezier_to_poly(bezier):
    # bezier to polygon
    u = np.linspace(0, 1, 20)
    bezier = bezier.reshape(2, 4, 2).transpose(0, 2, 1).reshape(4, 4)
    points = (
        np.outer((1 - u) ** 3, bezier[:, 0])
        + np.outer(3 * u * ((1 - u) ** 2), bezier[:, 1])
        + np.outer(3 * (u ** 2) * (1 - u), bezier[:, 2])
        + np.outer(u ** 3, bezier[:, 3])
    )
    points = np.concatenate((points[:, :2], points[:, 2:]), axis=0)

    return points


def get_parser():
    parser = argparse.ArgumentParser(description="Detectron2 Demo")
    parser.add_argument(
        "--config-file",
        default="configs/quick_schedules/e2e_mask_rcnn_R_50_FPN_inference_acc_test.yaml",
        metavar="FILE",
        help="path to config file",
    )
    parser.add_argument("--webcam", action="store_true",
                        help="Take inputs from webcam.")
    parser.add_argument("--video-input", help="Path to video file.")
    parser.add_argument("--input", nargs="+",
                        help="A list of space separated input images")
    parser.add_argument(
        "--output",
        help="A file or directory to save output visualizations. "
        "If not given, will show output in an OpenCV window.",
    )
    parser.add_argument(
        "--output_file",
        help="A file or directory to save output points. "
        "If not given, will show output in an OpenCV window.",
    )

    parser.add_argument(
        "--confidence-threshold",
        type=float,
        default=0.5,
        help="Minimum score for instance predictions to be shown",
    )
    parser.add_argument(
        "--opts",
        help="Modify config options using the command-line 'KEY VALUE' pairs",
        default=[],
        nargs=argparse.REMAINDER,
    )
    return parser


if __name__ == "__main__":
    mp.set_start_method("spawn", force=True)
    args = get_parser().parse_args()
    logger = setup_logger()
    logger.info("Arguments: " + str(args))

    cfg = setup_cfg(args)

    demo = VisualizationDemo(cfg)
    txt_file_path = args.output_file
    with open(txt_file_path, 'w') as file_result:
        if args.input:
            if os.path.isdir(args.input[0]):
                args.input = [os.path.join(args.input[0], fname)
                              for fname in os.listdir(args.input[0])]
            elif len(args.input) == 1:
                args.input = glob.glob(os.path.expanduser(args.input[0]))
                assert args.input, "The input path(s) was not found"
            for path in tqdm.tqdm(args.input, disable=None):
                # use PIL, to be consistent with evaluation
                img = read_image(path, format="BGR")
                predictions, points, texts = demo.run_on_image(img)

                # Print label?
                # print(texts)

                if len(predictions["instances"]) > 0:
                    image_name = path.split("/")[-1]
                    output_file_path = "predicted/" + image_name + ".txt"
                    print(output_file_path)

                    with open(output_file_path, 'w') as single_res:
                        Submit(output_file_path, points, texts, single_res)
                    single_res.close()
                #   logger.info(
                #       "{}: detected {} instances".format(
                #           path, len(predictions["instances"]))
                #   )
    file_result.close()
    sys.exit()

    if args.input:
        if os.path.isdir(args.input[0]):
            args.input = [os.path.join(args.input[0], fname)
                          for fname in os.listdir(args.input[0])]
        elif len(args.input) == 1:
            args.input = glob.glob(os.path.expanduser(args.input[0]))
            assert args.input, "The input path(s) was not found"
        for path in tqdm.tqdm(args.input, disable=not args.output):
            # use PIL, to be consistent with evaluation
            img = read_image(path, format="BGR")
            start_time = time.time()
            predictions, visualized_output = demo.run_on_image(img)
            logger.info(
                "{}: detected {} instances in {:.2f}s".format(
                    path, len(predictions["instances"]
                              ), time.time() - start_time
                )
            )
            ins = predictions["instances"]
            for i in range(0, len(predictions["instances"])):
                # print(str(i) + " height:")
                # print((ins[i])["fields"])
                # print(ins[i].get("beziers"))
                print("Number " + str(i))
                bez = ins[i].beziers.cpu().numpy()
                print(bez)
                points = _bezier_to_poly(bez)
                print(points)

            # for ins in predictions["instances"]:
            #     print(ins)

            if args.output:
                if os.path.isdir(args.output):
                    assert os.path.isdir(args.output), args.output
                    out_filename = os.path.join(
                        args.output, os.path.basename(path))
                else:
                    assert len(
                        args.input) == 1, "Please specify a directory with args.output"
                    out_filename = args.output
                visualized_output.save(out_filename)
            else:
                cv2.imshow(
                    WINDOW_NAME, visualized_output.get_image()[:, :, ::-1])
                if cv2.waitKey(0) == 27:
                    break  # esc to quit
    elif args.webcam:
        assert args.input is None, "Cannot have both --input and --webcam!"
        cam = cv2.VideoCapture(0)
        for vis in tqdm.tqdm(demo.run_on_video(cam)):
            cv2.namedWindow(WINDOW_NAME, cv2.WINDOW_NORMAL)
            cv2.imshow(WINDOW_NAME, vis)
            if cv2.waitKey(1) == 27:
                break  # esc to quit
        cv2.destroyAllWindows()
    elif args.video_input:
        video = cv2.VideoCapture(args.video_input)
        width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
        height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
        frames_per_second = video.get(cv2.CAP_PROP_FPS)
        num_frames = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
        basename = os.path.basename(args.video_input)

        if args.output:
            if os.path.isdir(args.output):
                output_fname = os.path.join(args.output, basename)
                output_fname = os.path.splitext(output_fname)[0] + ".mkv"
            else:
                output_fname = args.output
            assert not os.path.isfile(output_fname), output_fname
            output_file = cv2.VideoWriter(
                filename=output_fname,
                # some installation of opencv may not support x264 (due to its license),
                # you can try other format (e.g. MPEG)
                fourcc=cv2.VideoWriter_fourcc(*"x264"),
                fps=float(frames_per_second),
                frameSize=(width, height),
                isColor=True,
            )
        assert os.path.isfile(args.video_input)
        for vis_frame in tqdm.tqdm(demo.run_on_video(video), total=num_frames):
            if args.output:
                output_file.write(vis_frame)
            else:
                cv2.namedWindow(basename, cv2.WINDOW_NORMAL)
                cv2.imshow(basename, vis_frame)
                if cv2.waitKey(1) == 27:
                    break  # esc to quit
        video.release()
        if args.output:
            output_file.release()
        else:
            cv2.destroyAllWindows()
